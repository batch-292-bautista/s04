--A
SELECT DISTINCT name, id FROM artists WHERE name like "%d%";

--B
SELECT * FROM songs WHERE length < 330 ORDER BY id ASC;

--C
SELECT  DISTINCT album_title AS album_name, song_name, length
FROM albums JOIN songs ON albums.id = songs.album_id;

--D
SELECT artists.id, artists.name, albums.* FROM albums JOIN artists ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

--E
SELECT DISTINCT * FROM albums ORDER BY id DESC LIMIT 4;

--F
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;

--G
SELECT artists.id, artists.name, albums.* FROM albums JOIN artists ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";